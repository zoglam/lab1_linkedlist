/*
 *  Задание #6
 *  Автор: 
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "list.h"
#include "map.h"

void undecorate_name(char * name)
{
    int cnt;
    while (name[cnt]) {
        if (name[cnt] == '_')
            name[cnt] = ' ';
        cnt++;
    }
}

int main(int argc, char * argv[])
{
    COUNTRY ** map;

    /* Загрузка списка */
    map = map_load();

    char *cmd;
    char name[256];
    int population;
    int area;

    while(1)
    {
        printf("---------------------------------------");
        printf("\nUse syntax: \nadd\t<name> <popl> <area>");
        printf("\ndelete\t<name>");
        printf("\nview\t<name>");
        printf("\ndump, save or quit\n");
        printf("---------------------------------------");

        printf("\ncmd> ");
        scanf("%s", cmd);

        if (strcmp(cmd, "add") == 0) 
        {
            scanf("%s", name);
            undecorate_name(name);
            scanf("%d", &population);
            scanf("%d", &area);
            map_add(map, name, population, area);
        } 
        else if(strcmp(cmd, "delete") == 0)
        {
            scanf("%s", name);
            undecorate_name(name);
            map_delete(map, name);
        }
        else if(strcmp(cmd, "view") == 0)
        {
            scanf("%s", name);
            undecorate_name(name);
            COUNTRY* p = map_find(map, name);
            if(p != NULL)
                print_country(p);
        }
        else if(strcmp(cmd, "dump") == 0)
        {
            map_dump(map);
        }
        else if(strcmp(cmd, "save") == 0)
        {
            map_save(map);
        }
        else if(strcmp(cmd, "quit") == 0)
        {
            break;
        }
    }

    /* Удаление списка из динамической памяти */    
    map_clear(map);

    return 0;
}
