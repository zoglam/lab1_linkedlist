/*
 *  Задание #6
 *  Автор: 
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "list.h"

int main(int argc, char * argv[])
{
    COUNTRY * list;

    /* Загрузка списка */
    list = load();

    if(argc <= 1 || argc > 5)
    {
        printf("Use syntax: \n\tcountry add\t<name> <popl> <area>");
        printf("\n\tcountry delete\t<name>");
        printf("\n\tcountry count");
        printf("\n\tcountry dump\t<flag>");
        printf("\n\tcountry view\t<name>");
    }
    else if (!strcmp(argv[1], "add")) 
    {
        add(&list, argv[2], atoi(argv[3]), atoi(argv[4]));  
        save(list);
    }
    else if (!strcmp(argv[1], "delete")) 
    {
        COUNTRY *p = find(list, argv[2]); 
        if(p)       
        {
            delete(&list, p);
            save(list);
        }            
        else
            printf("Error");
        
    }
    else if (!strcmp(argv[1], "count")) 
    {
        printf("Count: %d", count(list));
    }
    else if (!strcmp(argv[1], "dump")) 
    {
        if(argc <= 2)
        {
            dump(list); 
        }
        else if(!strcmp(argv[2], "-n"))
        {
            sort_by_name(&list);
            dump(list); 
        }
        else if(!strcmp(argv[2], "-a"))
        {
            sort_by_area(&list);
            dump(list); 
        }
        else if(!strcmp(argv[2], "-p"))
        {
            sort_by_population(&list);
            dump(list); 
        } 
    }
    else if (!strcmp(argv[1], "view")) 
    {
        COUNTRY* p = find(list, argv[2]);
        if(p)
            print_country(p);
        else
            printf("Error");
    }  
    else
    {
        printf("Use syntax: \n\tcountry add\t<name> <popl> <area>");
        printf("\n\tcountry delete\t<name>");
        printf("\n\tcountry count");
        printf("\n\tcountry dump\t<flag>");
        printf("\n\tcountry view\t<name>");
    }
    /* Удаление списка из динамической памяти */
    clear(list);

    return 0;
}
